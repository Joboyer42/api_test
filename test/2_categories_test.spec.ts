
import test from 'japa'
import supertest from 'supertest'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`

let AdminToken = ''

test.group('test API categories controller', async () => {

    test('Ensure route create category work', async () => {
        await supertest(BASE_URL).post('/admin/categories')
        .auth(AdminToken, { type: 'bearer' })
            .field(
                {
                    title: "New Category",
                })
            .expect(401)
    })

})
